const express = require('express');
const app = express();


app.get("/", function (_req, res){
    //show this file when the "/" is requested
    res.sendFile(__dirname+"/views/index.html");
});


app.listen(3000, () => {
  console.log('Server started on port 3000');
});
